
import os


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=0, length=100, fill='█', printEnd="\r"):

    percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                     (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)

    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def main():
    GB = 1024**3
    novel = open("novel.txt", "r")
    bigFile = open("bigFile.txt", "a")
    bigFileRead = open('bigFile.txt', "r")
    bigFile.write(novel.read())
    bigFileSize = os.path.getsize("./bigFile.txt") / (GB)
    MAX_FILE_SIZE = 3  # File size in GB
    print(f"Creating a file of size {MAX_FILE_SIZE} GB")
    printProgressBar(0, MAX_FILE_SIZE, prefix='Progress:',
                     suffix='Complete', length=50)

    while (bigFileSize < MAX_FILE_SIZE):
        bigFile.write("\n"+bigFileRead.read())
        bigFileSize = os.path.getsize("./bigFile.txt") / (GB)
        printProgressBar(bigFileSize, MAX_FILE_SIZE, prefix='Progress:',
                         suffix='Complete', length=50)


main()
