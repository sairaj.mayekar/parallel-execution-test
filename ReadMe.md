# Setup

The scripts are used to demonstrate how parallelization can be used to speed up execution.
The goal of each script is to count occurrences of all words in a text file and write the result to a db

&nbsp;

## Create a big text file

Run the createBigFile script with `python3 createBigFile.py` you can specify the size of file by editing the MAX_FILE_SIZE constant

&nbsp;

## Python Count Words script - wordCountPython.py

This script does not utilize parallelization and runs action on one line of file at a time as it reads it.


### Run command

`python3 wordCountPython.py`

### Requirements

- Python v3
- PostgreSql v14

&nbsp;

## Spark Count Words script - wordCountSpark.py

This script uses spark to run tasks on the text file in parallel on multiple cores on multiple worker machines. You can view the execution of this application on localhost:4040.

### Run command

`spark-submit --driver-class-path postgresql-42.5.0.jar wordCountSpark.py`

### Requirements

- Python v3
- Spark v3
- PostgreSql v14

&nbsp;

## Polars Count Words script - wordCountPolars.py
This script uses polars lib to run tasks on the text file in parallel on all cores of a single machine

### Run command

`python3 wordCountPolars.py`

### Requirements

- Python v3
- Polars lib
- PostgreSql v14
