from operator import index
from psycopg2 import sql, connect

from time import process_time

t1_start = process_time()


# Print iterations progress
def printProgressBar(iteration, total, prefix='', suffix='', decimals=0, length=100, fill='█', printEnd="\r"):

    percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                     (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)

    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


def main():
    # Open the file in read mode
    text = open("bigFile.txt", "r")

    # Create an empty dictionary
    d = dict()

    # Loop through each line of the file
    print("Processing text file")
    totalLines = 63956942  # total number of lines for a 3 gb file

    printProgressBar(0, totalLines, prefix='Progress:',
                     suffix='Complete', length=50)
    for (lineIndex, line) in enumerate(text):
        printProgressBar(lineIndex, totalLines, prefix='Progress:',
                         suffix='Complete', length=50)
        # Remove the leading spaces and newline character
        line = line.strip()

        # Convert the characters in line to
        # lowercase to avoid case mismatch
        line = line.lower()

        # Split the line into words
        words = line.split()

        # Iterate over each word in line
        for word in words:

            # Check if the word is already in dictionary
            word = word.replace('"', "")
            word = word.replace("'", "")

            if (word == ''):
                continue
            if word in d:
                d[word] = d[word] + 1
            else:
                d[word] = 1

    print("Writing data to db")
    conn = connect(
        database="postgres", user='postgres', password='overlord', host='127.0.0.1', port='5432'
    )
    cursor = conn.cursor()
    table_id = "wordcount2"

    cols = ['word', 'count']
    # create column names from dataframe
    columns = sql.SQL(",").join(
        sql.Identifier(name) for name in cols)

    # create placeholders for the values. These will be filled later
    values = sql.SQL(",").join([sql.Placeholder() for _ in cols])

    # prepare the insert query
    insert_stmt = sql.SQL("INSERT INTO {} ({}) VALUES({});").format(
        sql.Identifier(table_id), columns, values
    )

    delete_stmt = f"DELETE FROM {table_id} where id > 0"
    cursor.execute(delete_stmt)
    cursor.executemany(insert_stmt, d.items())
    conn.commit()
    print("Data saved to db")


main()

t1_stop = process_time()

print("Elapsed time during the whole program in seconds:",
      t1_stop-t1_start)
