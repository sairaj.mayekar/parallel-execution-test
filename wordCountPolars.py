import polars as pl
from psycopg2 import sql
import psycopg2.extras
from time import process_time

t1_start = process_time()


def main():
    print(pl.threadpool_size())
    # load all lines in of file in dataframe
    df = pl.read_csv('./bigFile.txt', has_header=False, new_columns=['value'])
    print("Text in Data frame")
    print(df)

    # clean up the data
    df = df.select(
        pl.col('value').str.to_lowercase().str.split(by=" ").alias('word')
    )
    # remove empty words and load all words in a new row
    df = df.explode("word").filter(pl.col("word") != '')
    print("All words in file")
    print(df)

    # count number of words
    df = df.groupby("word").count()
    print("Number of occurrences of each word in file")
    print(df)

    # db connection properties
    uri = "postgresql://postgres:overlord@localhost:5432/postgres"
    table_id = "wordcountpolars"

    # create column names from dataframe
    columns = sql.SQL(",").join(sql.Identifier(name) for name in df.columns)
    # create placeholders for the values. These will be filled later
    values = sql.SQL(",").join([sql.Placeholder() for _ in df.columns])

    # prepare the insert query
    insert_stmt = sql.SQL("INSERT INTO {} ({}) VALUES({});").format(
        sql.Identifier(table_id), columns, values
    )

    # Remove old data from db
    delete_stmt = f"Delete from {table_id} where id > 0"

    conn = psycopg2.connect(uri)
    cur = conn.cursor()
    cur.execute(delete_stmt)
    psycopg2.extras.execute_batch(cur, insert_stmt, df.rows())
    conn.commit()


main()

t1_stop = process_time()

print("Elapsed time during the whole program in seconds:",
      t1_stop-t1_start)
