from pyspark.sql import SparkSession
from pyspark.sql import functions as func
from pyspark import SparkConf, SparkContext
import time

conf = SparkConf().setMaster(
    "local-cluster[4,4,1024]").set(
        'spark.driver.extraClassPath', '~/Downloads/postgresql-42.5.0.jar'
).set(
    "spark.jars", "./postgresql-42.5.0.jar"
)
spark = SparkSession.builder.appName(
    "WordCount").config(conf=conf).getOrCreate()

# Read each line of book into a dataframe
inputDF = spark.read.text("./bigFile.txt")

# Split using a regular expression that extracts words
words = inputDF.select(func.explode(
    func.split(inputDF.value, "\\W+")).alias("word"))
wordsWithoutEmptyString = words.filter(words.word != "")


# Normalize everything to lowercase
lowercaseWords = wordsWithoutEmptyString.select(
    func.lower(wordsWithoutEmptyString.word).alias("word"))

# Data frame with all words from file
lowercaseWords.show()

# Count up the occurrences of each word
wordCounts = lowercaseWords.groupBy("word").count()


wordCounts.write \
    .format("jdbc") \
    .mode("append") \
    .option("url", "jdbc:postgresql://localhost:5432/postgres") \
    .option("dbtable", "wordCount1") \
    .option("user", "postgres") \
    .option("password", "overlord") \
    . save()

time.sleep(86400000)
